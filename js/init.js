import YendorsTilesLibrary from "./tiles_library/YendorsTilesLibrary.js";
import YendorsTilesCollection from "./tiles_library/YendorsTilesCollection.js";

Hooks.on("init", () => {

	console.log("Initializing Yendor's Tiles Library")

	void YendorsTilesLibrary.init();
})

Hooks.on("canvasReady", (canvas) => {

	// Tile-Collection für die Szene holen
	const sceneTiles = YendorsTilesLibrary.sceneRegistry.get(canvas.scene.id);

	// Gibt es eine Collection und sie wurde noch nicht initialisiert, dann los!
	if (sceneTiles instanceof YendorsTilesCollection ?? !sceneTiles?.isInitialized()) {
		sceneTiles.forEach((tile) => {
			tile.init();
		})
	}
})