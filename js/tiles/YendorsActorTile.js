import YendorsDocumentTile from "./YendorsDocumentTile.js";

export default class YendorsActorTile extends YendorsDocumentTile {

	toggleTile(clickType) {
		super.toggleTile(clickType)
	}

	openDocument() {
		game.actors.get(this.documentID)?.sheet.render(true, {focus: true});
	}
}