import YendorsToggleTile, {ClickType} from "./YendorsToggleTile.js";

export default class YendorsDocumentTile extends YendorsToggleTile {

	_actionType = ClickType.LEFT;

	constructor(tileID, documentID) {
		super(tileID, true);
		this._documentID = documentID;
	}

	get documentID() {
		return this._documentID;
	}

	set documentID(value) {
		this._documentID = value;
	}

	toggleTile(clickType) {
		super.toggleTile(clickType);
	}

	clickLeft() {
		if (this._actionType === ClickType.LEFT) {
			this.openDocument();
		}
	}

	clickLeft2() {
		if (this._actionType === ClickType.LEFT_DOUBLE) {
			this.openDocument();
		}
	}

	clickRight() {
		if (this._actionType === ClickType.RIGHT) {
			this.openDocument();
		}	}

	clickRight2() {
		if (this._actionType === ClickType.RIGHT_DOUBLE) {
			this.openDocument();
		}	}

	openDocument() {};
}