import YendorsToggleTile from "./YendorsToggleTile.js";

export default class YendorsDoorTile extends YendorsToggleTile {

	constructor(tileID, doorID, showHoverPointer = false) {
		super(tileID, showHoverPointer);
		this._doorID = doorID;
	}

	get doorID() {
		return this._doorID;
	}

	_door = null;

	get door() {
		return this._door;
	}

	set door(value) {
		this._door = value;
	}

	getStoreID() {
		return this.doorID;
	}

	init() {
		super.init();
		if (!this.door) {
			const door = canvas?.scene?.walls?.get(this.doorID) ?? null;
			if (door) {
				this._door = door;
			}
		}
	}

	isInitialized() {
		if (!super.isInitialized()) {
			return false;
		}

		return this.light !== null;
	}

	trigger() {

		if (!game.user?.isGM) {
			return;
		}

		this.tile.update({
			hidden: this.door.ds !== 1,
			alpha: this.door.ds === 1 ? 1 : 0,
		})
	}

	clickLeft2() {}
}