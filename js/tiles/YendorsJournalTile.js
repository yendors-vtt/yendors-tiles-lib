import YendorsDocumentTile from "./YendorsDocumentTile.js";

export default class YendorsJournalTile extends YendorsDocumentTile {

	constructor(tileID, documentID, compendium = null) {
		super(tileID, documentID);
		this._compendium = compendium;
	}

	get compendium() {
		return this._compendium;
	}

	toggleTile(clickType) {
		super.toggleTile(clickType)
	}

	async openDocument() {
		if (this.compendium) {
			const compendium = await game.packs.get(this.compendium);
			const document = await compendium.getDocument(this.documentID);
			document?.sheet?.render(true, {focus: true});
		} else {
			game.journal.get(this.documentID)?.sheet.render(true, {focus: true});
		}
	}
}