import YendorsToggleTile from "./YendorsToggleTile.js";

export default class YendorsLightTile extends YendorsToggleTile {

	constructor(tileID, lightID, showHoverPointer = true) {
		super(tileID, showHoverPointer);
		this._lightID = lightID;
	}

	get lightID() {
		return this._lightID;
	}

	_light = null;

	get light() {
		return this._light;
	}

	set light(value) {
		this._light = value;
	}

	init() {
		super.init();
		if (!this.light) {
			const light = canvas?.scene?.lights?.get(this.lightID) ?? null;
			if (light) {
				this._light = light;
			}
		}
	}

	isInitialized() {
		if (!super.isInitialized()) {
			return false;
		}

		return this.light !== null;
	}

	clickLeft2() {
		super.clickLeft2();
		this.light.update({
			hidden: this.isHidden()
		})
	}
}