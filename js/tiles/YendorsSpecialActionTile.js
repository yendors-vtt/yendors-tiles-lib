import YendorsToggleTile from "./YendorsToggleTile.js";

export default class YendorsSpecialActionTile extends YendorsToggleTile {

	constructor(tileID, isClickable, specialAction) {
		super(tileID, isClickable);
		this._specialAction = specialAction;
	}

	toggleTile(clickType = ClickType.LEFT_DOUBLE) {
		// super.toggleTile(clickType);
		this._specialAction();
	}
}