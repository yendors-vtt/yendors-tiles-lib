import YendorsTilesLibrarySettings from "../tiles_library/YendorsTilesLibrarySettings.js";

export const ClickType = {
	LEFT: 'click',
	LEFT_DOUBLE: 'dblclick',
	RIGHT: 'rightclick',
	RIGHT_DOUBLE: 'dblrightclick',
	TRIGGER: 'trigger',
}

export default class YendorsToggleTile {

	constructor(tileID, isClickable = true) {
		this._tileID = tileID;
		this._isClickable = isClickable;

		// Macro-Funktion ablegen
		this.init();
	}

	getStoreID() {
		return this.tileID;
	}

	get tileID() {
		return this._tileID;
	}

	_tile = null;

	get isClickable() {
		return this._isClickable;
	}

	set isClickable(value) {
		this._isClickable = value;
	}

	get tile() {
		return this._tile;
	}

	set tile(value) {
		this._tile = value;
	}

	isHidden() {
		return !this.tile.hidden;
	}

	tileUpdate() {
		this.tile.update({
			hidden: this.isHidden(),
			alpha: this.isHidden() ? 0 : 1,
		})
	}

	init() {
		if (!this.tile) {
			const tile = canvas?.scene?.tiles?.get(this.tileID) ?? null;
			if (tile instanceof TileDocument) {
				tile.update({locked: true});
				this._tile = tile;
			}
			game.yendorsTilesLibrary.macros[this.tileID] = this.runMacro
			game.yendorsTilesLibrary.tiles[this.tileID] = this;
		}
	}

	isInitialized() {
		return this.tile !== null
	}

	runMacro = (clickType = ClickType.LEFT_DOUBLE) => {

		// Bei entsprechender Einstellung, wird ein Makro nur ausgeführt, wenn man sich auf der Szene befindet
		if (YendorsTilesLibrarySettings.macrosOnScene() && !game.yendorsTilesLibrary.sceneRegistry.get(canvas.scene?.id)?.get(this.tileID)) {
			return;
		}

		if (!this.isClickable) {
			clickType = ClickType.TRIGGER;
		}
		this.toggleTile(clickType);
	}

	toggleTile(clickType = ClickType.LEFT_DOUBLE) {

		// Noch nicht initialisiert? Dann mach mal ...
		if (!this.isInitialized()) {
			this.init();
		}

		// ... immer noch nicht? Dann ist hier Schluss
		if (!this.isInitialized()) {
			return;
		}


		switch (clickType) {
			case ClickType.LEFT_DOUBLE:
				this.clickLeft2();
				break;
			case ClickType.LEFT:
				this.clickLeft();
				break;
			case ClickType.RIGHT_DOUBLE:
				this.clickRight2();
				break;
			case ClickType.RIGHT:
				this.clickRight();
				break;
			case ClickType.TRIGGER:
				this.trigger();
				break;
		}
	}

	clickLeft() {};

	clickLeft2() {
		if (this.isClickable) {
			this.tileUpdate();
		}
	};

	clickRight() {};

	clickRight2() {};

	trigger() {
		this.tileUpdate();
	}

}