import YendorsToggleTile from "./YendorsToggleTile.js";

export default class YendorsWindowTile extends YendorsToggleTile {

	_window = null;
	_windowLockTile = null;

	_windowLockTileID = null;

	constructor(tileID, windowID, windowLockTileID) {
		super(tileID, true);
		this._windowID = windowID;
		this._windowLockTileID = windowLockTileID;
	}

	init() {
		if (this.isBreakable())
		{
			super.init();
		}
		if (!this.window) {
			const window = canvas?.scene?.walls?.get(this.windowID) ?? null;
			if (window) {
				this._window = window;
			}
		}
		if (this.isLockable() && !this.windowLockTile) {
			const lockTile = canvas?.scene?.tiles?.get(this.windowLockTileID) ?? null;
			if (lockTile) {
				this._windowLockTile = lockTile;
			}
		}
	}

	updateWindow(willBeLocked, willBeBroken) {
		// Standard-Fenster-Werte
		let move = 20;
		let sound = 20;
		let light = 0;
		let sight = 0;

		console.log('isBroken', willBeBroken);
		console.log('isLocked', willBeLocked);
		console.log('lockTile', this.windowLockTile.hidden);

		if (willBeBroken && !willBeLocked) {
			move = 0;
			sound = 0;
		}

		if (willBeLocked) {
			light = 20;
			sight = 20;
		}

		this.window.update({
			move: move,
			sound: sound,
			light: light,
			sight: sight
		})
	}

	getStoreID() {
		if (this.tileID) {
			return this.tileID;
		}

		return this.tileID ?? this.windowLockTileID;
	}

	isBreakable() {
		return this.tileID !== null;
	}

	isLockable() {
		return this.windowLockTileID !== null;
	}

	isBroken() {
		if (!this.isBreakable()) {
			return false;
		}
		return !this.tile.hidden;
	}

	isLocked() {
		if (!this.isLockable()) {
			return false;
		}
		return !this.windowLockTile.hidden;
	}

	get windowID() {
		return this._windowID;
	}

	set windowID(value) {
		this._windowID = value;
	}

	get window() {
		return this._window;
	}

	set window(value) {
		this._window = value;
	}

	get windowLockTile() {
		return this._windowLockTile;
	}

	set windowLockTile(value) {
		this._windowLockTile = value;
	}

	get windowLockTileID() {
		return this._windowLockTileID;
	}

	set windowLockTileID(value) {
		this._windowLockTileID = value;
	}

	clickLeft2() {
		if (this.isBreakable()) {

		super.clickLeft2();
			this.updateWindow(this.isLocked(), !this.isBroken());
		}
	}

	clickRight() {
		if (this.isLockable()) {

			const isLocked = this.isLocked();
			const isBroken = this.isBroken();

			this.windowLockTile.update({
				hidden: !this.windowLockTile.hidden
			});
			this.updateWindow(!this.isLocked(), this.isBroken());
		}
	}
}