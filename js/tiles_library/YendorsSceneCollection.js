export default class YendorsSceneCollection extends Collection {

	_isInitialized = false;

	get isInitialized() {
		return this._isInitialized;
	}

	set isInitialized(value) {
		this._isInitialized = value;
	}
}