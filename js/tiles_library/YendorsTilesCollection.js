import YendorsToggleTile from "../tiles/YendorsToggleTile.js";

export default class YendorsTilesCollection extends Collection {

	set(key, value) {
		if (value instanceof YendorsToggleTile) {
			super.set(key, value);
		}
	}
}