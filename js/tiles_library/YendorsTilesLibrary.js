import YendorsLightTile from "../tiles/YendorsLightTile.js";
import YendorsToggleTile, {ClickType} from "../tiles/YendorsToggleTile.js";
import YendorsDoorTile from "../tiles/YendorsDoorTile.js";
import YendorsSceneCollection from "./YendorsSceneCollection.js";
import YendorsTilesCollection from "./YendorsTilesCollection.js";
import YendorsWindowTile from "../tiles/YendorsWindowTile.js";
import YendorsActorTile from "../tiles/YendorsActorTile.js";
import YendorsSpecialActionTile from "../tiles/YendorsSpecialActionTile.js";
import YendorsTilesLibrarySettings from "./YendorsTilesLibrarySettings.js";
import YendorsJournalTile from "../tiles/YendorsJournalTile.js";

export default class YendorsTilesLibrary {

	static moduleID = 'yendors-tiles-lib';

	static hoveredTiles = [];
	static sceneRegistry = new YendorsSceneCollection();
	static macros = {};
	static tiles = new YendorsTilesCollection();
	static userActors = [];

	_sceneID = null;


	get sceneID() {
		return this._sceneID;
	}

	set sceneID(value) {
		this._sceneID = value;
	}

	constructor(sceneID) {
		this._sceneID = sceneID;
	}

	static createScene(sceneId) {

		YendorsTilesLibrary.sceneRegistry.set(sceneId, new YendorsTilesCollection());
		return new YendorsTilesLibrary(sceneId);
	}

	static async init() {

		try {
			YendorsTilesLibrarySettings.init();

			console.log("Init Yendor's Tiles Library");
			game.yendorsTilesLibrary = this;

			let lastPosition = undefined;

			Hooks.once('setup', () => {
				game.actors.forEach((actor) => {
					if (actor.permission > 0) {
						YendorsTilesLibrary.userActors.push(actor.id);
					}
				})
			})


			document.body.addEventListener("mousemove", function () {

				let mouse = canvas?.app?.renderer?.events?.pointer ?? canvas?.app?.renderer?.plugins?.interaction?.mouse ?? canvas?.app?.renderer?.plugins?.interaction?.pointer;
				YendorsTilesLibrary.hoveredTiles = [];

				if (!mouse) {
					return;
				}

				const currentPosition = mouse.getLocalPosition(canvas.app.stage);

				if (!lastPosition) {
					lastPosition = currentPosition;
					return;
				}

				if (!canvas.scene) {
					return;
				}

				if (!(canvas.activeLayer instanceof TokenLayer)) {
					return;
				}

				let showPointer = false;
				YendorsTilesLibrary.getCurrentTileCollection().forEach(tile => {
					if (!tile.isClickable) {
						return;
					}

					const isJournalTile = (tile instanceof YendorsJournalTile)

					if (YendorsTilesLibrary.isCursorInTile(lastPosition, tile)) {
						if (!isJournalTile && !(tile instanceof YendorsActorTile) && !(tile instanceof YendorsSpecialActionTile) && !game.user?.isGM) {
							return;
						}
						if (!isJournalTile && !YendorsTilesLibrary.userActors.includes(tile.documentID) && !(tile instanceof YendorsSpecialActionTile) && !game.user?.isGM) {
							return;
						}

						showPointer = true;
						YendorsTilesLibrary.hoveredTiles.push(tile);
					}
				});

				if (showPointer) {
					$('#board').css({ cursor: 'pointer' });
				} else {
					$('#board').css({ cursor: '' });
				}

				lastPosition = currentPosition;

			})

			// Linksklick
			let _tileOnLeftClick = function (wrapped, ...args) {
				let event = args[0];
				canvasClick.call(this, event, 'click');
				wrapped(...args);
			}
			const oldClickLeft = TokenLayer.prototype._onClickLeft;
			TokenLayer.prototype._onClickLeft = function (event) {
				return _tileOnLeftClick.call(this, oldClickLeft.bind(this), ...arguments);
			}

			// Doppelklick
			let _tileOnLeftClick2 = function (wrapped, ...args) {
				let event = args[0];
				canvasClick.call(this, event, 'dblclick');
				wrapped(...args);
			}
			const oldClickLeft2 = TokenLayer.prototype._onClickLeft2;
			TokenLayer.prototype._onClickLeft2 = function (event) {
				return _tileOnLeftClick2.call(this, oldClickLeft2.bind(this), ...arguments);
			}

			// Rechtsklick
			let _tileOnRightClick = function (wrapped, ...args) {
				let event = args[0];
				canvasClick.call(this, event, 'rightclick');
				wrapped(...args);
			}
			const oldClickRight = TokenLayer.prototype._onClickRight;
			TokenLayer.prototype._onClickRight = function (event) {
				return _tileOnRightClick.call(this, oldClickRight.bind(this), ...arguments);
			}

			// RechtsDoppelklick
			let _tileOnRightClick2 = function (wrapped, ...args) {
				let event = args[0];
				canvasClick.call(this, event, 'dblrightclick');
				wrapped(...args);
			}
			const oldClickRight2 = TokenLayer.prototype._onClickRight2;
			TokenLayer.prototype._onClickRight2 = function (event) {
				return _tileOnRightClick2.call(this, oldClickRight2.bind(this), ...arguments);
			}

			// KLICK!
			let canvasClick = function (event, clicktype) {
				let maxZindex = 0;
				let clickableTiles = []
				YendorsTilesLibrary.hoveredTiles.forEach((tile) => {
					if (tile.tile.z > maxZindex) {
						clickableTiles = [tile];
						maxZindex = tile.tile.z;
					} else if (tile.tile.z === maxZindex) {
						clickableTiles.push(tile);
					}
				});
				clickableTiles.forEach((tile) => {
					tile.toggleTile(clicktype);
				});
			};

		} catch (e) {
			console.log(e)
		}
	}

	static isCursorInTile(position, tile) {

		if (!tile.tile) return false;

		const x = tile.tile.x;
		const y = tile.tile.y;
		const xEnd = x + tile.tile.width;
		const yEnd = y + tile.tile.height;

		return position.x >= x && position.x <= xEnd && position.y >= y && position.y <= yEnd
	}

	addLightTile(tileID, lightID) {
		this.addTile(new YendorsLightTile(tileID, lightID));
	}

	addDoorTile(tileID, doorID) {
		// Türen werden als Key unter der dooID abgelegt, da sie von der Tür getriggert werden
		this.addTile(new YendorsDoorTile(tileID, doorID));
	}

	addWindowTile(tileID, windowID, windowLockTileID = null) {
		this.addTile(new YendorsWindowTile(tileID, windowID, windowLockTileID));
	}

	addToggleTile(tileID, isClickable = true) {

		this.addTile(new YendorsToggleTile(tileID, isClickable));
	}

	addSpecialActionTile(tileID, isClickable, specialAction) {

		this.addTile(new YendorsSpecialActionTile(tileID, isClickable, specialAction))
	}

	addActorTile(tileID, actorID) {

		this.addTile(new YendorsActorTile(tileID, actorID));
	}

	addJournalTile(tileID, journalID, compendiumString = null) {

		this.addTile(new YendorsJournalTile(tileID, journalID, compendiumString));
	}

	createCharacterJournalTile() {

	}

	addTile(tile) {

		YendorsTilesLibrary.sceneRegistry.get(this.sceneID).set(tile.getStoreID(), tile);
	}

	static getCurrentTileCollection() {

		const sceneCollection = YendorsTilesLibrary.sceneRegistry.get(canvas.scene.id);

		if (sceneCollection) {
			return sceneCollection;
		}

		return new YendorsTilesCollection();
	}
}

Hooks.on('updateWall', async (document) => {

	if (document.door) {
		YendorsTilesLibrary.sceneRegistry.get(canvas.scene.id)?.get(document.id)?.toggleTile(ClickType.TRIGGER);
	}
});