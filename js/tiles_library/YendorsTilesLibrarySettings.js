import YendorsTilesLibrary from "./YendorsTilesLibrary.js";

export default class YendorsTilesLibrarySettings {

	static SETTING = {
		macrosOnScene: 'macrosOnScene'
	}

	static init() {

		console.log('REGISTER', YendorsTilesLibrary.moduleID);
		game.settings.register(YendorsTilesLibrary.moduleID, YendorsTilesLibrarySettings.SETTING.macrosOnScene, {
			name: 'Macros nur auf Szene',
			hint: 'Macros werden nur ausgeführt, wenn man sich auf der richtigen Szene befindet',
			scope: "world",
			config: true,
			type: Boolean,
			default: true,
			restricted: true,
		});
	}

	static macrosOnScene() {
		return game.settings.get(YendorsTilesLibrary.moduleID, YendorsTilesLibrarySettings.SETTING.macrosOnScene);
	}
}